header_width=2.54;
header_height=9;
pin_inset = 0.5;
inner_width = header_width - pin_inset*2;

module header_pins(pins, height=header_height) {
    for (a =[0:pins-1])
    {
        translate([header_width*a,0,0]) 
        {
           translate([pin_inset,pin_inset,0])
            {
                color("yellow") cube([inner_width,inner_width, height]);
            }
     
        }    
    }
}

module header_row(pins, pin_holes = false, height = header_height ) {
    
    difference() {
        {
        for (a =[0:pins-1])
        {
            translate([header_width*a,0,0]) 
            {
                color("blue") cube([header_width,header_width,height]);
                
            }
            
        }
        }
        if (pin_holes) {
                header_pins(pins);
        }
    }

}


//header_row(8,true);
