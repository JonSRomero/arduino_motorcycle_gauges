include <headers.scad>

module pin_cutout(thickness=2) {

    translate([3,6,0]) {
    //row_space();
    
    header_row(pins=8);
    translate([0,header_width*2,0]) header_row(pins=8);
    }

    
}


module motorcycle_harness_connector_female() {
    
    long_outside = 26;
    short_outside = 19;
    height_outside = 16.15+3;
    height_inside = 16.15;
    
    floor_thickness = height_outside - height_inside;
    
    long_inside = 22;
    short_inside = 15;
    
    wall_thickness = 2;
    
    key_distance_from_wall = 1.47;
    key_thickness = 5.53;
    key_depth = 2.5;
 

difference() {

    
union(){    
    
    difference(){
        union() {
            cube([long_outside,short_outside,height_outside]);
            translate([long_outside,short_outside/2,1]) cylinder(h=2,r=5,center=true);
            translate([0,short_outside/2,1]) cylinder(h=2,r=5,center=true);
        }
        translate([wall_thickness,wall_thickness,floor_thickness]){
            cube([long_inside,short_inside,height_inside]);
        }
    }
    translate([wall_thickness + key_distance_from_wall,wall_thickness,0]) {
        cube([key_thickness,key_depth,height_outside]);
    }
    translate([wall_thickness+short_inside,wall_thickness,0]) {
        cube([key_thickness,key_depth,height_outside]);
    }
}

    
    pin_cutout();
    
    
}

}


motorcycle_harness_connector_female();