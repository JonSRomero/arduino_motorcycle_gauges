include <arduino.scad>
include <offset.scad>
include <pins.scad>
include <headers.scad>
include <motorcycle_harness_connectors.scad>

header_height=9;
header_width=2.54;

module base() {
linear_extrude(height = 2, convexity = 10)
   import (file = "outline.dxf", layer = "Base");
}

module surround() {

translate([0,0,0]) {
color("blue")  offset_shell(2) {
linear_extrude(height = 24, convexity = 10)
   import (file = "outline.dxf", layer = "Base");    
}
}

translate([70,70,2]) rotate(270) {
color("red") standoffs(height=10);
translate([0,0,10]) arduino();
}
}
/*
translate([36,36,0]){
    color("blue") gauge();
}
translate([105,47,0]){
    color("blue") gauge();
}

}

module gauge(radius=33,height=3)
{
    cylinder(r=radius,h=height);
}
*/



module support_deck() {
    translate([0,0,-header_height]) cube([26,19,header_height]);
}

module row_space() {
    cube([8*header_width,header_width,header_width]);
}
//color("blue") translate([header_width*i,0,0]) cube(header_width);

//arduino();
//test();
//difference() {


    
motorcycle_harness_connector_female();

    


//}
//surround();