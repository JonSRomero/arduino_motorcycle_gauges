
default_depth=1;
base_width = 10;
base_height = 4;

module base()
{
	cube([base_width, base_height, default_depth]);
}

module right_bevel()
{
	
	segment_count=180;
	right_bevel_radius=base_height-1;
	right_bevel_origin=[base_width-2.5,base_height*0.4,default_depth*0.5];
	translate(right_bevel_origin) {
	   cylinder(r=right_bevel_radius,h=default_depth,center=true, $fn=segment_count);
	}
}

module cuts(){
	translate([0,base_height,-0.1])
	{
		cube([base_width, base_height, default_depth*2]);
	
	}
	translate([base_width*4/5,base_height,0])
	{
		rotate(-25) {
			cube(5);
		}
		rotate(90+25) {
			cube(5);
		}
	}

}

module adds()
{

		right_bevel();
		base();

}

difference() {
adds();
cuts();
}

