a = 0;
b = 1;
c = 2;
d = 3;
e = 4;
f = 5;

A=[0,0,0];
B=[0,3,0];
C=[3,0,0];
D=[0,0,1];
E=[0,3,1];
F=[3,0,1];


//my_faces = [[a,b,c],[d,f,e],[a,c,f,d],[c,b,e,f],[a,d,e,b]];
my_triangles = [[a,b,c],[d,f,e],
					[d,a,c],[c,f,d],
					[b,e,c],[c,e,f],
					[b,a,d],[e,b,d]];


polyhedron(points=[
					A,B,C,D,E,F
				],
				triangles = my_triangles);


