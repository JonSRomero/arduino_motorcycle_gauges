/*
Z750S Motorcycle Gauge Cluster
 */

//#define LIGHTS_DIGITAL
#define LIGHTS_ANALOG

#define CNT_V 1024.0 / 5.0

//Test mode pin


#define TEST_MODE_ENABLE_PIN 3


#define FUEL_LEVEL_TEST_PIN A5
#define RPM_TEST_PIN         A4

//Water statuses
#define WATER_STATUS_GOOD 0
#define WATER_STATUS_HIGH 1
#define WATER_STATUS_OVER 2

//Pin assignments

#ifdef LIGHTS_DIGITAL 

  #define TACH_INTERRUPT 0
  #define TACH_PIN 2
  
  //12ish volts when on
    //Pin connected to output of voltage divider
  #define LEFT_TURN_SIGNAL_PIN          11
  
    //Pin connected to output of voltage divider
  #define RIGHT_TURN_SIGNAL_PIN         10
  
    //Pin connected to output of voltage divider
  #define HIGH_BEAM_INDICATOR_PIN         12
  
  //Resistors measured via voltage divider circuit
  #define FUEL_SENSOR_5V_PIN        8
  #define FUEL_SENSOR_PIN           A0
  #define FUEL_SENSOR_REF_V         5
  #define FUEL_SENSOR_R_KNOWN_OHM   270
  
  #define WATER_SENSOR_5V_PIN        7
  #define WATER_SENSOR_PIN          A1
  #define WATER_SENSOR_REF_V        5
  #define WATER_SENSOR_R_KNOWN_OHM  270
  
  
  //sent to gnd when indicated
  #define FI_WARNING_PIN            A3 
  #define NEUTRAL_INDIATOR_PIN      A2
  #define OIL_WARNING_PIN           9 
#else
//LIGHTS ANALOG (simpler board)

  #define TACH_INTERRUPT 0
  #define TACH_PIN 2
  
  //12ish volts when on
    //Pin connected to output of voltage divider
  #define LEFT_TURN_SIGNAL_PIN          A3
  #define LIGHT_THRESHOLD               256
  
    //Pin connected to output of voltage divider
  #define RIGHT_TURN_SIGNAL_PIN         A5
  
    //Pin connected to output of voltage divider
  #define HIGH_BEAM_INDICATOR_PIN       A2
  
  //Resistors measured via voltage divider circuit
  #define FUEL_SENSOR_5V_PIN        4
  #define FUEL_SENSOR_PIN           A0
  #define FUEL_SENSOR_REF_V         5
  #define FUEL_SENSOR_R_KNOWN_OHM   152
  
  #define WATER_SENSOR_5V_PIN        7
  #define WATER_SENSOR_PIN          A1
  #define WATER_SENSOR_REF_V        5
  #define WATER_SENSOR_R_KNOWN_OHM  270
  
  
  //sent to gnd when indicated
  #define FI_WARNING_PIN            6
  #define NEUTRAL_INDIATOR_PIN      5
  #define OIL_WARNING_PIN           7 

#endif

unsigned int fuel_level_divider_counts;
unsigned int water_temp_divider_counts;

#ifdef LIGHTS_ANALOG
unsigned int left_turn_signal_divider_counts;
unsigned int right_turn_signal_divider_counts;
unsigned int high_beam_divider_counts;
#endif



volatile unsigned long tach_pulses;
unsigned long rpm;

//volatile unsigned long speed_pulses;
//unsigned long mph;


float getVoltsFromCounts(unsigned int counts, float ref)
{
     return (float)counts * ref / 1024.0;
}

/*

|-------R1----
Vin          * Vout
|            |
|            R2
|            |
|------------|
      |
     GND

*/

float solveDividerForR2(float vin, float vout, float r1)
{
  // vout = vin * Runknown/(Rknown+Runknown)
  // Runknown = Rknown  * vout/(Vin - Vout)
  return r1* vout / (vin - vout);
}

int get_fuel_level_percent(int counts)
{
  float signal_voltage = getVoltsFromCounts(counts, 5.0f);
  
  int resistance = constrain( solveDividerForR2(5.0f, signal_voltage, (float)FUEL_SENSOR_R_KNOWN_OHM),
                                0.0f,
                                75.0f
                                );
                                
  return map(resistance, 0, 75, 100, 0);
  
}

int get_water_status(int counts)
{
  float signal_voltage = getVoltsFromCounts(counts, 5.0f);
  
  float resistance = solveDividerForR2(5.0f, signal_voltage, (float)WATER_SENSOR_R_KNOWN_OHM);

  if (resistance > 21.2 && resistance < 28.2)
  {
    return WATER_STATUS_OVER;
  }
  else if (resistance > 15.2 && resistance < 21.2)
  {
     return WATER_STATUS_HIGH;
  }
  else
  {
     return WATER_STATUS_GOOD;
  }
  
  return 0.0f;
}


void increment_tach_pulses()
{
   tach_pulses++; 
}
/*
void increment_speed_pulses()
{
   speed_pulses++; 
}
*/

unsigned long compute_frequency(  unsigned long current_pulses,
                                  unsigned long delta_us )
{
  unsigned long result;

  result = current_pulses * 1000000 /  delta_us;

  return result;
}
/*
unsigned long compute_speed_from_frequency( unsigned long frequency)
{
   //I do not know the ratio, for now just use same ratio as Tach
   return frequency * (6000l / 200l); 
}
*/
unsigned long compute_rpm_from_frequency( unsigned long frequency)
{
   //200 HZ maps to 6000 RPM 
   
   return frequency * (6000l / 200l);
}

void compute_rpm()
{
  static unsigned long last_processed = 0l;  
  unsigned long current_time = micros();
  unsigned long hz;
  
  hz = compute_frequency(tach_pulses, current_time - last_processed);

  last_processed = current_time;
  tach_pulses = 0;

  rpm = compute_rpm_from_frequency( hz );
      
  return;
}
/*
void compute_speed()
{

}
*/
// the setup routine runs once when you press reset:
void setup() {              
  
  fuel_level_divider_counts        = 0;
  water_temp_divider_counts        = 0; 
 
  tach_pulses = 0;
  
  //speed_pulses = 0;

  // Set up test mode enable pin
  pinMode( TEST_MODE_ENABLE_PIN,          INPUT_PULLUP);

#ifdef LIGHTS_DIGITAL
  //Set left, right, and high beam pins to INPUT_PULLUP
  pinMode( LEFT_TURN_SIGNAL_PIN,      INPUT_PULLUP);
  pinMode( RIGHT_TURN_SIGNAL_PIN,     INPUT_PULLUP);
  pinMode( HIGH_BEAM_INDICATOR_PIN,   INPUT_PULLUP);
#endif  
  //Set fi, oil, neutral pins for pullup
  pinMode( FI_WARNING_PIN,       INPUT_PULLUP);
  pinMode( OIL_WARNING_PIN,      INPUT_PULLUP);
  pinMode( NEUTRAL_INDIATOR_PIN, INPUT_PULLUP);

  //Configure FUEL and WATER 5V pins for output
  pinMode( FUEL_SENSOR_5V_PIN,    OUTPUT);
  digitalWrite( FUEL_SENSOR_5V_PIN, HIGH);
  /*
  pinMode( WATER_SENSOR_5V_PIN, OUTPUT );
  digitalWrite( WATER_SENSOR_5V_PIN, LOW);
  */
  //Set tach pin to input pullup
  pinMode(TACH_PIN, INPUT_PULLUP);
  
  //Interrupt 0 is on pin 2
  //Interrupt 1 is on pin 3
  attachInterrupt(TACH_INTERRUPT, increment_tach_pulses, FALLING);
  
  // Provide power out on 13 for bluetooth module
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  
  Serial.begin(9200);   

}

int neutral_indicator_on;
int fi_warning_on;
int oil_warning_on;
int high_beam_on;
int left_turn_signal_on;
int right_turn_signal_on;
int fuel_percent;

void emit_protocol()
{
  Serial.print("L");
  Serial.println(left_turn_signal_on);
  
  Serial.print("R"); 
  Serial.println(right_turn_signal_on);
  
  Serial.print("H"); 
  Serial.println(high_beam_on);

  Serial.print("G");
  Serial.println( fuel_percent ); 

/*
  Serial.print("WATER "); 
  Serial.println(get_water_temp(water_temp_divider_counts));
*/
  Serial.print("F"); 
  Serial.println(fi_warning_on);

  Serial.print("O"); 
  Serial.println(oil_warning_on);

  Serial.print("N"); 
  Serial.println(neutral_indicator_on);

  Serial.print("R"); 
  rpm = constrain(rpm,0,99999);
  Serial.print(rpm);
  Serial.println();
  
}

void capture_data_production()
{
  //Update analog values (eventually wont do this every loop)
  fuel_level_divider_counts        = analogRead( FUEL_SENSOR_PIN );
  water_temp_divider_counts        = analogRead( WATER_SENSOR_PIN );
 
  //update digital values
  // these values are true when input is low.
#ifdef LIGHTS_DIGITAL  
  left_turn_signal_on  =  ( LOW == digitalRead( LEFT_TURN_SIGNAL_PIN    )       );
  right_turn_signal_on =  ( LOW == digitalRead( RIGHT_TURN_SIGNAL_PIN   )       );
  high_beam_on         =  ( LOW == digitalRead( HIGH_BEAM_INDICATOR_PIN )       );
#else 
//Analog light readings
  left_turn_signal_divider_counts = analogRead( LEFT_TURN_SIGNAL_PIN );
  right_turn_signal_divider_counts = analogRead( RIGHT_TURN_SIGNAL_PIN );
  high_beam_divider_counts = analogRead( HIGH_BEAM_INDICATOR_PIN );
  
  left_turn_signal_on =  ( left_turn_signal_divider_counts > LIGHT_THRESHOLD ); 
  right_turn_signal_on = ( right_turn_signal_divider_counts > LIGHT_THRESHOLD ); 
  high_beam_on =      ( high_beam_divider_counts > LIGHT_THRESHOLD ); 

#endif
  
  fi_warning_on        = ( LOW == digitalRead( FI_WARNING_PIN )       );
  oil_warning_on       = ( LOW == digitalRead( OIL_WARNING_PIN )      );
  neutral_indicator_on = ( LOW == digitalRead( NEUTRAL_INDIATOR_PIN ) );

  fuel_percent = get_fuel_level_percent(fuel_level_divider_counts);

//compute_speed();
  compute_rpm();
  
}


void capture_data_test()
{
  
  //update digital values
  // these values are true when input is low.
  left_turn_signal_on  =  ( LOW == digitalRead( LEFT_TURN_SIGNAL_PIN    )       );
  right_turn_signal_on =  ( LOW == digitalRead( RIGHT_TURN_SIGNAL_PIN   )       );
  high_beam_on         =  ( LOW == digitalRead( HIGH_BEAM_INDICATOR_PIN )       );
  
  fi_warning_on        = ( LOW == digitalRead( FI_WARNING_PIN )       );
  oil_warning_on       = ( LOW == digitalRead( OIL_WARNING_PIN )      );
  neutral_indicator_on = ( LOW == digitalRead( NEUTRAL_INDIATOR_PIN ) );

  
  fuel_percent  = map(analogRead(FUEL_LEVEL_TEST_PIN),0,1023,0,100);
  rpm           = map(analogRead(RPM_TEST_PIN),0,1023,0,16000);
  
}

// the loop routine runs over and over again forever:
void loop() {

  int test_mode_enabled;
  
  test_mode_enabled = (LOW == digitalRead( TEST_MODE_ENABLE_PIN ));
  
  if (test_mode_enabled)
  {
    capture_data_test();
  }
  else
  {
     capture_data_production(); 
  }
  
  // Emit protocol in both cases
  emit_protocol();
  

  delay(100);
}
