/*
  Analog Input
 Demonstrates analog input by reading an analog sensor on analog pin 0 and
 turning on and off a light emitting diode(LED)  connected to digital pin 13. 
 The amount of time the LED will be on and off depends on
 the value obtained by analogRead(). 
 
 The circuit:
 * Potentiometer attached to analog input 0
 * center pin of the potentiometer to the analog pin
 * one side pin (either one) to ground
 * the other side pin to +5V
 * LED anode (long leg) attached to digital output 13
 * LED cathode (short leg) attached to ground
 
 * Note: because most Arduinos have a built-in LED attached 
 to pin 13 on the board, the LED is optional.
 
 
 Created by David Cuartielles
 modified 30 Aug 2011
 By Tom Igoe
 
 This example code is in the public domain.
 
 http://arduino.cc/en/Tutorial/AnalogInput
 
 */
 
#include <EEPROM.h>

#define OUTPUT_PIN 2
#define LED 13

#define MIN_MODE 1
#define MODE_100_HZ 1
#define MODE_200_HZ 2
#define MODE_300_HZ 3
#define MODE_400_HZ 4
//#define MODE_SINE_400HZ 5
//#define MODE_CYCLE_400HZ 6
#define MAX_MODE 4

byte mode;


void setup() {
  unsigned int rate;
  
  mode = EEPROM.read(0);

  if (mode < (MIN_MODE + 1) || mode > MAX_MODE) 
  { 
     mode = MAX_MODE + 1;
  }
  mode = mode - 1;
  EEPROM.write(0,mode);

  switch(mode)
  {
      case MODE_100_HZ:
        rate = 100;
        break;
      case MODE_200_HZ:
        rate = 200;
        break;
      case MODE_300_HZ:
        rate = 300;      
        break;
      case MODE_400_HZ:
        rate = 400;
        break;
   
  }

  tone(OUTPUT_PIN, rate);

  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  
}


void loop() {
  //flash the mode on startup
  int i = 0;

  digitalWrite(LED,LOW);
  for(i = 0; i < mode;i++)
  {

     delay(200);
     digitalWrite(LED,HIGH); 
     delay(200);
     digitalWrite(LED, LOW);
  }


  delay(2000);   
      
}
