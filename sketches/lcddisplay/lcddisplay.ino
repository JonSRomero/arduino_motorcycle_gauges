/*********************

Example code for the Adafruit RGB Character LCD Shield and Library

This code displays text on the shield, and also reads the buttons on the keypad.
When a button is pressed, the backlight changes color.

**********************/

// include the library code:
#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include <Adafruit_RGBLCDShield.h>

// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

// These #defines make it easy to set the backlight color
#define RED 0x1
#define YELLOW 0x3
#define GREEN 0x2
#define TEAL 0x6
#define BLUE 0x4
#define VIOLET 0x5
#define WHITE 0x7

uint8_t i=0;
int fi_warning = 0;
int oil_warning = 0;
int neutral_indicator = 0;
int fuel_level_pct = 0;
char rpm_string[6];
char fuel_pct_string[4];
int rpm = 0;
int fuel_pct = 0;

char buffer[128];
int buffer_pos = 0;

int stringComplete;  // whether the string is complete


void setup() {
  // Debugging output
  Serial.begin(9600);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);

  lcd.setBacklight(WHITE);
  init_static();
  rpm_string[0] = (char)0;
  rpm = 0;
  fuel_pct_string[0] = (char)0;
  fuel_pct = 0;
  
  // reserve 200 bytes for the buffer:
  buffer_pos = 0;
  buffer[0] = (char)0;
  
}




void init_static(){
  lcd.clear();
 /*
 "0123456789ABCDEF"
 "FI _ OIL _ NTR _" 
 "G:012% 12000 RPM"
 
 */
 lcd.setCursor(0,0);
 lcd.print( "FI _ OIL _ NTR _" );
 lcd.setCursor(0,1);
 lcd.print( "G:    %      RPM");
}

void update_display()
{

  // Update fuel injection warning
  lcd.setCursor(3, 0);
 
  if (fi_warning)
  {
    lcd.print("1"); 
  }
  else
  {
    lcd.print("0"); 
  }


  // Update Oil Warning
  lcd.setCursor(9, 0);
  if (oil_warning)
  {
    lcd.print("1"); 
  }
  else
  {
    lcd.print("0");
  }

  // Update NTRL 
  lcd.setCursor(15, 0);
  if (neutral_indicator)
  {
    lcd.print("1"); 
  }
  else
  {
    lcd.print("0"); 
  }
  
  // Update FUEL
  if (fuel_pct < 1000 && fuel_pct >= 100)
  {
    lcd.setCursor(2,1);
    lcd.print( fuel_pct );
  }
  if (fuel_pct < 100 && fuel_pct >= 10)
  {
    lcd.setCursor(2,1);
    lcd.print( " " );
    lcd.setCursor(3,1);
    lcd.print( fuel_pct );
  }
  if (fuel_pct < 10 && fuel_pct >= 0)
  {
    lcd.setCursor(2,1);
    lcd.print( "  " );
    lcd.setCursor(4,1);

    lcd.print( fuel_pct );
  }
 
  
  
  // Update RPM
   // Print blank portion

  if (rpm < 100000 && rpm >=10000)
  {
    lcd.setCursor(7,1);
    lcd.print(rpm); 
  }
  else if (rpm < 10000 && rpm >= 1000)
  {
    lcd.setCursor(7,1);
    lcd.print(" ");
    lcd.setCursor(8,1);
    lcd.print(rpm);
  }
  else if (rpm < 1000 && rpm >= 100)
  {
    lcd.setCursor(7,1);
    lcd.print("  ");
    lcd.setCursor(9,1);
    lcd.print(rpm);
    
  }
  else if (rpm < 100 && rpm >= 10)
  {
    lcd.setCursor(7,1);
    lcd.print("   ");
    lcd.setCursor(10,1);
    lcd.print(rpm);
  }
  else if (rpm < 10 && rpm >= 0)
  {
    lcd.setCursor(7,1);
    lcd.print("    ");
    lcd.setCursor(11,1);
    lcd.print(rpm);
  }


}




/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  if (buffer_pos >= 127)
  {
     Serial.println("Warning, buffer overflow when reading protocol string"); 
     buffer_pos = 0;
  }
  
  while (Serial.available()) {
    // get the new byte:
    char inChar;
    
    inChar = (char)Serial.read(); 
    
    if (inChar == '\n' || inChar=='\r') {
      buffer[buffer_pos] = 0;
      process_message();
      buffer_pos = 0;
    }
    else
    {
      buffer[buffer_pos] = inChar;
      buffer_pos++;
    }

    
  }
}

void process_message()
{
    Serial.println(buffer);
        
    switch (buffer[0])
    {
      
      case 'R':
        rpm_string[0] = buffer[1];
        rpm_string[1] = buffer[2];
        rpm_string[2] = buffer[3];    
        rpm_string[3] = buffer[4];    
        rpm_string[4] = buffer[5];
        rpm_string[5] = 0;
        rpm = atoi(rpm_string);
        break;
     case 'F':
        fi_warning = (buffer[1] == '1');
        break;
     case 'O':
        oil_warning = (buffer[1] == '1');
        break;
     case 'N':
        neutral_indicator = (buffer[1] == '1');
        break;
     case 'G':
       fuel_pct_string[0] = buffer[1];
       fuel_pct_string[1] = buffer[2];
       fuel_pct_string[2] = buffer[3];
       fuel_pct_string[3] = 0;
       fuel_pct = atoi(fuel_pct_string);
       Serial.print ("Fuel STR: \"");
       Serial.print (fuel_pct_string);
       Serial.print ("\"");
       Serial.print (" Fuel %: " );
       Serial.println( fuel_pct );
       break;
     default:
        //Do nothing
        break;

    }
  
}

void loop() {
  int incoming_byte;
  char next_byte;
  int dirty = 1;
  
  update_display();    
  
  if (rpm > 11000)
  {
    lcd.setBacklight(RED);
    delay(50);
    lcd.setBacklight(WHITE);
    delay(10);
  }
  else
  {
    lcd.setBacklight(WHITE);
  }
/*
  uint8_t buttons = lcd.readButtons();

  if (buttons) {
    lcd.clear();
    lcd.setCursor(0,0);
    if (buttons & BUTTON_UP) {
      lcd.print("UP ");
      lcd.setBacklight(RED);
    }
    if (buttons & BUTTON_DOWN) {
      lcd.print("DOWN ");
      lcd.setBacklight(YELLOW);
    }
    if (buttons & BUTTON_LEFT) {
      lcd.print("LEFT ");
      lcd.setBacklight(GREEN);
    }
    if (buttons & BUTTON_RIGHT) {
      lcd.print("RIGHT ");
      lcd.setBacklight(TEAL);-
    }
    if (buttons & BUTTON_SELECT) {
      lcd.print("SELECT ");
      lcd.setBacklight(VIOLET);
    }
  }
*/
}
